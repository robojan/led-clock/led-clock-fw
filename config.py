#!/usr/bin/env python

import espefuse
from ecdsa import SigningKey, curves
import base64
import qrcode
import sys
import argparse
import os
import re
import pathlib
import zipfile
import shutil


def get_dpp_dir(args):
    return args.secret_dir.joinpath('dpp')


def create_dpp_key(dir, overwrite=False):
    path = os.path.join(dir, 'dpp.pem')
    with open(path, 'wb' if overwrite else 'xb') as f:
        key = SigningKey.generate(curve=curves.NIST256p)
        f.write(key.to_pem())


def load_dpp_key(dir):
    path = os.path.join(dir, 'dpp.pem')
    with open(path, 'rb') as f:
        return SigningKey.from_pem(f.read())


def generate_secrets(args):
    if args.overwrite and get_dpp_dir(args).exists():
        shutil.rmtree(get_dpp_dir(args))
    create_dpp_key(args.secret_dir, args.overwrite)


def import_secrets(args):
    if args.overwrite and get_dpp_dir(args).exists():
        shutil.rmtree(get_dpp_dir(args))
    with zipfile.ZipFile(args.file, 'r') as zip:
        for f in zip.namelist():
            dest = args.secret_dir.joinpath(f)
            if not args.overwrite and dest.exists():
                raise FileExistsError(f"File {f} already exists")
            zip.extract(f, args.secret_dir)


def export_secrets(args):
    with zipfile.ZipFile(args.file, 'w') as zip:
        for f in args.secret_dir.glob('**/*'):
            if f.name == 'README.md':
                continue
            relPath = f.relative_to(args.secret_dir)
            zip.write(f, relPath.as_posix())


def store_dpp_priv_key(path, key):
    with open(path, 'wt') as f:
        keyBytes = bytearray(key.to_string()).hex()
        f.writelines([
            '#pragma once\n',
            '\n',
            'static constexpr char kDppKey[] = "{}";\n'.format(keyBytes),
            'static constexpr char kDppChannels[] = "6";\n'
        ])


def get_mac(args):
    if args.mac:
        macBytes = args.mac
    else:
        esp = espefuse.get_esp(chip='esp32', port=args.port,
                               baud=args.baud, connect_mode='default_reset')
        macBytes = esp.read_mac()
    return bytearray(macBytes).hex(sep=':')


def save_qr(path, uri):
    qr = qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_L)
    qr.add_data(uri)
    qr.print_ascii()
    qr.make_image().save(path)


def save_uri(path, uri):
    with open(path, 'wt') as f:
        f.write(uri)


def generate_code(args):
    # Load the private key
    privKey = load_dpp_key(args.secret_dir)

    # Create the output directory
    os.makedirs(args.out_dir, exist_ok=True)

    # Generate code for DPP
    store_dpp_priv_key(args.out_dir.joinpath('dppkey.h'), privKey)


def generate_dpp(args):
    # Generate DPP dir
    dpp_dir = get_dpp_dir(args)
    os.makedirs(dpp_dir, exist_ok=True)

    # Load the private key
    privKey = load_dpp_key(args.secret_dir)

    # Generate the URI
    mac = get_mac(args)
    pub_key = base64.standard_b64encode(
        privKey.verifying_key.to_der(point_encoding='compressed')).decode('UTF-8')
    uri = "DPP:C:81/6;M:{};K:{};;".format(mac, pub_key)
    print("URI: {}".format(uri))

    # Store the results
    basename = mac.replace(':', '_')
    qr_file = dpp_dir.joinpath(f'{basename}.png')
    uri_file = dpp_dir.joinpath(f'{basename}.txt')
    save_qr(qr_file, uri)
    save_uri(uri_file, uri)


def mac_address(string):
    matches = re.match(
        '([0-9a-fA-F]{1,2})[:-]([0-9a-fA-F]{1,2})[:-]([0-9a-fA-F]{1,2})[:-]([0-9a-fA-F]{1,2})[:-]([0-9a-fA-F]{1,2})[:-]([0-9a-fA-F]{1,2})', string)
    if bool(matches):
        return [int(x, 16) for x in matches.group(1, 2, 3, 4, 5, 6)]
    else:
        raise argparse.ArgumentTypeError(
            f"{string} is not a valid MAC address")


def main():
    parser = argparse.ArgumentParser(
        description='Tool to generate the secrets')
    parser.add_argument('--secret_dir', type=pathlib.Path,
                        default='secrets', help='Path where the secret files are stored.')
    parser.add_argument('--out_dir', type=pathlib.Path, default='build/secret',
                        help='Output path where the generated files will be placed.')

    subparsers = parser.add_subparsers(
        dest='cmd', help='Commands', required=True)

    gensecret_parser = subparsers.add_parser(
        'gen-secrets', help='Generate secrets')
    gensecret_parser.add_argument(
        '-y', action='store_true', dest='overwrite', default=False, help='Overwrite output files')

    gencodeparser = subparsers.add_parser(
        'gen-code', help='Generate code')

    importsecret_parser = subparsers.add_parser(
        'import-secrets', help='import secrets')
    importsecret_parser.add_argument(
        'file', type=pathlib.Path, help='Secrets archive')
    importsecret_parser.add_argument(
        '-y', action='store_true', dest='overwrite', default=False, help='Overwrite output files')

    exportsecret_parser = subparsers.add_parser(
        'export-secrets', help='export secrets')
    exportsecret_parser.add_argument(
        'file', type=pathlib.Path, help='Secrets archive')

    dpp_parser = subparsers.add_parser(
        'gen-dpp', help='Generate Device Provisioning QR codes')
    macsrc_group = dpp_parser.add_mutually_exclusive_group(required=True)
    macsrc_group.add_argument(
        '--mac', type=mac_address, help='MAC address to generate for.')
    macsrc_group.add_argument(
        '--port', type=str, help='Port on which to retrieve the MAC address')
    dpp_parser.add_argument('--baud', default=115200,
                            type=int, help='Baudrate to use to talk to the ESP')

    args = parser.parse_args()

    os.makedirs(args.secret_dir, exist_ok=True)

    if args.cmd == 'gen-secrets':
        generate_secrets(args)
    elif args.cmd == 'gen-dpp':
        generate_dpp(args)
    elif args.cmd == 'gen-code':
        generate_code(args)
    elif args.cmd == 'import-secrets':
        import_secrets(args)
    elif args.cmd == 'export-secrets':
        export_secrets(args)


if __name__ == '__main__':
    main()
    sys.exit()
