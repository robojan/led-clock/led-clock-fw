// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 February 27
 * Author: Robbert-Jan de Jager
 *
 * Implementation of the matrix driver class
 ****************************************************************************/

#include "matrixdriver.h"

#include <driver/spi_master.h>
#include <driver/timer.h>
#include <userboard.h>
#include <priorities.h>
#include <esp_log.h>

const char *TAG = "Matrix";

MatrixDriver::MatrixDriver()
{
    initIo();
    initSpi();
    initTimer();
    initTask();
}

MatrixDriver::~MatrixDriver()
{
    deinitTimer();
    deinitSpi();
    deinitIo();
    deinitTask();
}

void MatrixDriver::start()
{
    // Send initial frame
    ioRst();
    send();

    // Start the timer
    ESP_ERROR_CHECK(timer_start(board::ledclock::timerGroup, board::ledclock::timerIdx));
}

void MatrixDriver::stop()
{
    // Stop the timer
    ESP_ERROR_CHECK(timer_pause(board::ledclock::timerGroup, board::ledclock::timerIdx));
}

void MatrixDriver::initSpi()
{
    // Initialize the peripheral from the board config
    spi_bus_config_t config{
        .mosi_io_num     = board::ledclock::dataPin,
        .miso_io_num     = -1,
        .sclk_io_num     = board::ledclock::sckPin,
        .data2_io_num    = -1,
        .data3_io_num    = -1,
        .data4_io_num    = -1,
        .data5_io_num    = -1,
        .data6_io_num    = -1,
        .data7_io_num    = -1,
        .max_transfer_sz = (board::ledclock::frameSize + 7) / 8,
        .flags = SPICOMMON_BUSFLAG_MASTER | SPICOMMON_BUSFLAG_SCLK | SPICOMMON_BUSFLAG_MOSI |
                 SPICOMMON_BUSFLAG_GPIO_PINS,
        .intr_flags = 0,
    };
    ESP_ERROR_CHECK(spi_bus_initialize(board::ledclock::spi, &config, SPI_DMA_DISABLED));

    spi_device_interface_config_t devconfig{
        .command_bits     = 0,
        .address_bits     = 0,
        .dummy_bits       = 0,
        .mode             = 0,
        .duty_cycle_pos   = 0,
        .cs_ena_pretrans  = 0,
        .cs_ena_posttrans = 0,
        .clock_speed_hz   = board::ledclock::freq,
        .input_delay_ns   = 0,
        .spics_io_num     = -1,
        .flags            = SPI_DEVICE_BIT_LSBFIRST,
        .queue_size       = 1,
        .pre_cb           = nullptr,
        .post_cb          = nullptr,
    };
    ESP_ERROR_CHECK(spi_bus_add_device(board::ledclock::spi, &devconfig, &_spi));
}

void MatrixDriver::deinitSpi()
{
    ESP_ERROR_CHECK(spi_bus_remove_device(_spi));

    ESP_ERROR_CHECK(spi_bus_free(board::ledclock::spi));
}

void MatrixDriver::initIo()
{
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::disDriversPin, 0));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::disDriversPin, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::disHandsPin, 0));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::disHandsPin, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::disScreenPin, 0));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::disScreenPin, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::latchPin, 0));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::latchPin, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::rstPin, 1));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::rstPin, GPIO_MODE_OUTPUT));
}

void MatrixDriver::deinitIo()
{
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::disDriversPin, GPIO_MODE_DISABLE));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::disHandsPin, GPIO_MODE_DISABLE));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::disScreenPin, GPIO_MODE_DISABLE));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::latchPin, GPIO_MODE_DISABLE));
    ESP_ERROR_CHECK(gpio_set_direction(board::ledclock::rstPin, GPIO_MODE_DISABLE));
}

void MatrixDriver::initTimer()
{
    timer_config_t config{
        .alarm_en    = TIMER_ALARM_EN,
        .counter_en  = TIMER_PAUSE,
        .intr_type   = TIMER_INTR_LEVEL,
        .counter_dir = TIMER_COUNT_UP,
        .auto_reload = TIMER_AUTORELOAD_EN,
        .divider     = kTimerDivider,
    };
    ESP_ERROR_CHECK(timer_init(board::ledclock::timerGroup, board::ledclock::timerIdx, &config));


    ESP_ERROR_CHECK(
        timer_set_counter_value(board::ledclock::timerGroup, board::ledclock::timerIdx, 0));

    static constexpr uint64_t kCounterValue = TIMER_BASE_CLK / kTimerDivider / kTimerFreq;

    ESP_ERROR_CHECK(timer_set_alarm_value(board::ledclock::timerGroup, board::ledclock::timerIdx,
                                          kCounterValue));

    ESP_ERROR_CHECK(timer_enable_intr(board::ledclock::timerGroup, board::ledclock::timerIdx));

    ESP_ERROR_CHECK(timer_isr_callback_add(
        board::ledclock::timerGroup, board::ledclock::timerIdx,
        [](void *self) { return reinterpret_cast<MatrixDriver *>(self)->onTimerCallback(); }, this,
        priorities::isr::driverTimer));
}

void MatrixDriver::deinitTimer()
{
    ESP_ERROR_CHECK(timer_disable_intr(board::ledclock::timerGroup, board::ledclock::timerIdx));
    ESP_ERROR_CHECK(
        timer_isr_callback_remove(board::ledclock::timerGroup, board::ledclock::timerIdx));
    ESP_ERROR_CHECK(timer_deinit(board::ledclock::timerGroup, board::ledclock::timerIdx));
}

void MatrixDriver::initTask()
{
    _taskCompleted = false;
    _taskRunning   = true;

    auto result =
        xTaskCreatePinnedToCore([](void *self) { reinterpret_cast<MatrixDriver *>(self)->task(); },
                                "MatrixDriver", 1024 * 3, this, priorities::task::matrixDriver,
                                &_task, priorities::task::matrixDriverAffinity);
    assert(result == pdPASS);
}

void MatrixDriver::deinitTask()
{
    _taskRunning = false;

    int timeout = pdMS_TO_TICKS(1000);

    while(!_taskCompleted && timeout >= 0)
    {
        vTaskDelay(1);
        timeout--;
    }
}

void MatrixDriver::createFrame()
{
    // Prepare for next frame
    // Update the current column position
    _handsDriver   = _handsDriver == board::ledclock::handCols - 1 ? 0 : _handsDriver + 1;
    _displayDriver = _displayDriver == board::ledclock::segCols - 1 ? 0 : _displayDriver + 1;

    createDriverFrame();
    createDisplayFrame();
    createHandsFrame();
}

void MatrixDriver::createDriverFrame()
{
    auto &frame = frontBuffer();
    auto  beg   = reinterpret_cast<uint8_t *>(frame.data() + (board::ledclock::columnsOffset / 8));
    auto  end   = beg + ((board::ledclock::cols + 7) / 8);

    // Clear all the drivers and then active the ones which are active this frame
    std::fill(beg, end, board::ledclock::disabledDriverValue);

    auto activateColumn = [&](unsigned idx) { beg[idx / 8U] ^= (0x80U >> (idx % 8U)); };

    if(_handsEnabled)
    {
        activateColumn(board::ledclock::handColSeq[_handsDriver]);
    }
    if(_displayEnabled)
    {
        activateColumn(board::ledclock::segColSeq[_displayDriver]);
    }
}

void MatrixDriver::createDisplayFrame()
{
    auto &frame = frontBuffer();
    auto  beg   = frame.data() + (board::ledclock::segmentsOffset / 8);
    auto  end   = beg + ((board::ledclock::ledsPerSeg + 7) / 8);

    if(renderDisplayCallback)
    {
        uint8_t colorIdx = _displayDriver / board::ledclock::numSeg;
        uint8_t segIdx   = _displayDriver % board::ledclock::numSeg;
        renderDisplayCallback(etl::span(beg, end), segIdx, colorIdx);
    }
    else
    {
        std::fill(beg, end, std::byte{board::ledclock::disabledSegmentValue});
    }
}

void MatrixDriver::createHandsFrame()
{
    auto &frame = frontBuffer();
    auto  beg   = frame.data() + (board::ledclock::handGroupOffset / 8);
    auto  end   = beg + ((board::ledclock::ledsPerGroup + 7) / 8);

    if(renderHandsCallback)
    {
        uint8_t colorIdx     = _handsDriver / board::ledclock::numHandGroups;
        uint8_t handGroupIdx = _handsDriver % board::ledclock::numHandGroups;
        renderHandsCallback(etl::span(beg, end), handGroupIdx, colorIdx);
    }
    else
    {
        std::fill(beg, end, std::byte{board::ledclock::disabledHandValue});
    }
}

void MatrixDriver::send()
{
    auto &frame = backBuffer();

    _spiTrans.length    = frame.size() * 8;
    _spiTrans.tx_buffer = frame.data();
    ESP_ERROR_CHECK(spi_device_polling_start(_spi, &_spiTrans, portMAX_DELAY));
}

void MatrixDriver::completeSend()
{
    ESP_ERROR_CHECK(spi_device_polling_end(_spi, portMAX_DELAY));
}

void MatrixDriver::ioLatch() const
{
    // Pulse the latch pin
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::latchPin, 1));
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::latchPin, 0));
}

void MatrixDriver::ioRst() const
{
    // Pulse the reset pin
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::rstPin, 0));
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::rstPin, 1));
}

void MatrixDriver::ioEnableDrivers(bool enable) const
{
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::disDriversPin, enable ? 0 : 1));
}

void MatrixDriver::ioEnableHands(bool enable) const
{
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::disHandsPin, enable ? 0 : 1));
}

void MatrixDriver::ioEnableScreen(bool enable) const
{
    ESP_ERROR_CHECK(gpio_set_level(board::ledclock::disScreenPin, enable ? 0 : 1));
}

bool MatrixDriver::onTimerCallback()
{
    bool hasTaskWoken = _timerSemaphore.release_from_isr();

    return hasTaskWoken;
}

void MatrixDriver::task()
{
    using namespace std::chrono_literals;
    while(_taskRunning)
    {
        // Wait for timer semaphore
        bool taken = _timerSemaphore.try_acquire_for(50ms);
        if(!taken)
            continue;

        completeSend();
        ioLatch();
        swapBuffers();
        send();
        createFrame();
    }
    _taskCompleted = true;
}
