// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 March 06
 * Author: Robbert-Jan de Jager
 *
 * This file contains the implementation of the display class
 ****************************************************************************/

#include "display.h"
#include "matrixdriver.h"
#include "userboard.h"

#include <esp_log.h>
#include <esp_err.h>

static const char *TAG = "Display";

Display::Display(std::shared_ptr<Configuration> config)
{
    // Add default displays and menus
    _displayMenu = std::make_unique<DisplayMenu>(std::move(config), *this);
    addMenu(_displayMenu.get());
    _disabledDisplay = std::make_unique<DisabledDisplay>();
    addDisplay(_disabledDisplay.get());

    initButtons();
}

void Display::registerDriver(MatrixDriver &driver)
{
    driver.renderDisplayCallback =
        etl::delegate<MatrixDriver::RenderFunc>::create<DisplayRenderer,
                                                        &DisplayRenderer::renderFrame>(_renderer);
}

void Display::tick()
{
    updateButtons();

    switch(_state)
    {
    default:
    case State::Display:
        if(_activeDisplay < _displayItems.size())
        {
            _displayItems[_activeDisplay]->render(_renderer);
        }
        break;
    case State::Menu:
        if(_activeMenu < _menuItems.size())
        {
            _menuItems[_activeMenu]->render(_renderer);
        }
        break;
    case State::MenuList:
        renderMenuList();
        break;
    }

    _renderer.tick();
}

void Display::addDisplay(IDisplayItem *item, bool active)
{
    _displayItems.push_back(item);

    // When active, change the active display to the just added option.
    if(active)
    {
        _activeDisplay = _displayItems.size() - 1;
    }

    // When this is the first item in the list, activate it.
    if(_displayItems.size() == 0)
    {
        _displayItems[0]->activateDisplay(_renderer);
    }
}

void Display::addMenu(IMenuItem *item)
{
    _menuItems.push_back(item);

    // When this is the first item in the list, activate it.
    if(_menuItems.size() == 0)
    {
        _menuItems[0]->activateMenu(_renderer);
    }
}

void Display::setActiveDisplay(int idx)
{
    // Clamp idx between 0 and displayItems.size() -1. If the size is 0 then the idx will also be 0.
    // This is valid because the rest of the class will check if the idx if valid.
    if(idx >= _displayItems.size())
    {
        idx = _displayItems.size() - 1;
    }
    if(idx < 0)
    {
        idx = 0;
    }

    if(_state == State::Display)
    {
        if(_activeDisplay < _displayItems.size())
        {
            _displayItems[_activeDisplay]->deactivateDisplay(_renderer);
        }
        _activeDisplay = idx;
        _displayItems[_activeDisplay]->activateDisplay(_renderer);
    }
    else
    {
        _activeDisplay = idx;
    }
}

void Display::renderMenuList()
{
    // If we are out of range. Draw an error message.
    if(_activeMenu >= _menuItems.size())
    {
        _renderer.setColor(1, 0, 0);
        _renderer.setText("Error");
        return;
    }

    // Do not redraw when active menu has not changed.
    if(_lastMenu == _activeMenu)
    {
        return;
    }

    _lastMenu = _activeMenu;

    _renderer.setColor(1, 1, 1);
    _renderer.setText(_menuItems[_activeMenu]->menuName());
}

void Display::initButtons()
{
    ESP_ERROR_CHECK(gpio_set_pull_mode(board::buttons::changeBtn, GPIO_PULLUP_ONLY));
    ESP_ERROR_CHECK(gpio_set_direction(board::buttons::changeBtn, GPIO_MODE_INPUT));

    ESP_ERROR_CHECK(gpio_set_pull_mode(board::buttons::applyBtn, GPIO_PULLUP_ONLY));
    ESP_ERROR_CHECK(gpio_set_direction(board::buttons::applyBtn, GPIO_MODE_INPUT));
}

void Display::updateButtons()
{
    bool applyState    = gpio_get_level(board::buttons::applyBtn) == 0;
    bool changeState   = gpio_get_level(board::buttons::changeBtn) == 0;
    bool applyChanged  = _applyBtnDebounce.add(applyState);
    bool changeChanged = _changeBtnDebounce.add(changeState);

    if(applyChanged)
    {
        bool apply = _applyBtnDebounce.is_set();
        if(apply)
        {
            processApplyButton(_applyBtnDebounce.is_held());
        }
    }

    if(changeChanged)
    {
        bool change = _changeBtnDebounce.is_set();
        if(change)
        {
            processChangeButton(_changeBtnDebounce.is_held());
        }
    }

    _applySet  = _applyBtnDebounce.is_set();
    _changeSet = _changeBtnDebounce.is_set();
}

void Display::processApplyButton(bool hold)
{
    // If in the menu pass the button through
    switch(_state)
    {
    case State::Menu:
        // We send the apply signal and then go back to the display state
        if(_activeMenu < _menuItems.size())
        {
            _menuItems[_activeMenu]->apply(hold);
            _menuItems[_activeMenu]->deactivateMenu(_renderer);
        }
        _state = State::Display;
        ESP_LOGI(TAG, "Going to display state");
        if(_activeDisplay < _displayItems.size())
        {
            _displayItems[_activeDisplay]->activateDisplay(_renderer);
        }
        break;
    case State::Display:
        // Ignore apply button
        break;
    case State::MenuList:
        // activate the menu

        _state = State::Menu;
        ESP_LOGI(TAG, "Going to menu %d", _activeMenu);
        _lastMenu = -1;
        if(_activeMenu < _menuItems.size())
        {
            _menuItems[_activeMenu]->activateMenu(_renderer);

            // Press apply button again to do the action
            if(_menuItems[_activeMenu]->isAction())
            {
                processApplyButton(false);
            }
        }
        break;
    }
}

void Display::processChangeButton(bool hold)
{
    // If in the menu pass the button through
    switch(_state)
    {
    case State::Menu:
        if(_activeMenu < _menuItems.size())
        {
            _menuItems[_activeMenu]->change(hold);
        }
        break;
    case State::Display:
        // Go the menu list
        if(_activeDisplay < _displayItems.size())
        {
            _displayItems[_activeDisplay]->deactivateDisplay(_renderer);
        }
        _state = State::MenuList;
        ESP_LOGI(TAG, "Going to the menu list");
        break;
    case State::MenuList:
        // Cycle through the available menu options
        _activeMenu = (_activeMenu + 1) % _menuItems.size();
        break;
    }
}
