// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 February 28
 * Author: Robbert-Jan de Jager
 *
 * Implementation of the ClockRenderer class
 ****************************************************************************/

#include "clockrenderer.h"

#include <userboard.h>

void ClockRenderer::renderFrame(etl::span<std::byte> buffer, uint8_t groupIdx, uint8_t colorIdx)
{
    auto &fb = frontBuffer();

    for(int subHandIdx = 0; subHandIdx < board::ledclock::handsPerGroup; subHandIdx++)
    {
        int handIdx = groupIdx * board::ledclock::handsPerGroup + subHandIdx;
        assert(handIdx >= 0 && handIdx < fb.size());
        auto &hand = fb[handIdx];

        uint8_t ledVal = board::ledclock::disabledHandValue;
        for(int ledIdx = 0; ledIdx < board::ledclock::ledsPerHand; ledIdx++)
        {
            if((hand.color[ledIdx] & (1 << colorIdx)) != 0)
            {
                ledVal ^= (0x80 >> ledIdx);
            }
        }

        buffer[subHandIdx] = static_cast<std::byte>(ledVal);
    }
}


void ClockRenderer::clear()
{
    auto &fb = backBuffer();
    for(int sec = 0; sec < fb.size(); sec++)
    {
        auto &hand = fb[sec];
        for(int r = 0; r < hand.color.size(); r++)
        {
            hand.color[r] = 0;
        }
    }
}

void ClockRenderer::drawHand(int sec, int startR, int endR, uint8_t r, uint8_t g, uint8_t b)
{
    auto &fb = backBuffer();
    assert(sec >= 0 && sec < fb.size());
    auto &hand = fb[sec];
    if(endR < startR)
        std::swap(endR, startR);

    assert(startR >= 0 && startR < hand.color.size());
    assert(endR >= 0 && endR <= hand.color.size());

    auto col = rgbToColor(r, g, b);

    for(int radius = startR; radius < endR; radius++)
    {
        hand.color[radius] = col;
    }
}

void ClockRenderer::setLed(int sec, int radius, uint8_t r, uint8_t g, uint8_t b)
{
    auto &fb = backBuffer();
    assert(sec >= 0 && sec < fb.size());
    auto &hand = fb[sec];
    assert(radius >= 0 && radius < hand.color.size());

    hand.color[radius] = rgbToColor(r, g, b);
}

uint8_t ClockRenderer::rgbToColor(uint8_t r, uint8_t g, uint8_t b)
{
    uint8_t col = 0;
    if(r != 0)
        col |= 1 << 0;
    if(g != 0)
        col |= 1 << 1;
    if(b != 0)
        col |= 1 << 2;
    return col;
}
