// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 February 28
 * Author: Robbert-Jan de Jager
 *
 * Implementation of the DisplayRenderer class
 ****************************************************************************/

#include "displayrenderer.h"

#include <userboard.h>

void DisplayRenderer::renderFrame(etl::span<std::byte> buffer, uint8_t charIdx, uint8_t colorIdx)
{
    assert(charIdx < _buffer.size());
    auto &segment = _buffer[charIdx];

    auto setChar = [&](uint16_t val)
    {
        buffer[0] = static_cast<std::byte>((int{val} & 0xFF));
        buffer[1] = static_cast<std::byte>(((int{val} >> 8) & 0xFF));
    };

    if(segment.color[colorIdx] != 0)
    {
        auto c = segment.character;
        assert(c <= board::ledclock::charmap.size());
        setChar(board::ledclock::charmap[c]);
    }
    else
    {
        setChar(board::ledclock::disabledSegmentValue);
    }
}

void DisplayRenderer::tick()
{
    auto t      = xTaskGetTickCount();
    auto tDelta = t - _lastScrollT;
    if(tDelta >= kScrollPeriodTicks)
    {
        _lastScrollT = t;

        int maxScroll =
            std::max(0, static_cast<int>(_displayStr.size()) - static_cast<int>(_buffer.size()));
        int minScroll = 0;

        // Try one step
        _scrollPos += _scrollInc;

        // If we are past the end of the scroll region, swap the direction and correct the step.
        if(_scrollPos < minScroll || _scrollPos > maxScroll)
        {
            _scrollInc *= -1;
            _scrollPos += _scrollInc;
        }

        updateBuffer();
    }
}

void DisplayRenderer::setText(std::string_view str)
{
    _displayStr = str;
    _scrollPos  = 0;
    _scrollInc  = -1;
}

void DisplayRenderer::setColor(uint8_t r, uint8_t g, uint8_t b)
{
    for(int i = 0; i < _buffer.size(); i++)
    {
        _buffer[i].color[0] = b;
        _buffer[i].color[1] = g;
        _buffer[i].color[2] = r;
    }
}

void DisplayRenderer::updateBuffer()
{
    for(int i = 0; i < _buffer.size(); i++)
    {
        int  idx             = i + _scrollPos;
        char c               = idx >= _displayStr.size() ? ' ' : _displayStr[idx];
        _buffer[i].character = static_cast<uint8_t>(c);
    }
}
