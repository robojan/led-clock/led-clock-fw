// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 February 28
 * Author: Robbert-Jan de Jager
 *
 * Declaration of clock rendererer class
 ****************************************************************************/
#pragma once

#include <array>
#include <userboard.h>
#include <etl/span.h>

class ClockRenderer
{
public:
    void renderFrame(etl::span<std::byte> buffer, uint8_t charIdx, uint8_t colorIdx);

    void clear();
    void drawHand(int sec, int startR, int endR, uint8_t r, uint8_t g, uint8_t b);
    void setLed(int sec, int radius, uint8_t r, uint8_t g, uint8_t b);
    void swapBuffer() { _activeBuffer = (_activeBuffer + 1) & 1; }

private:
    struct Hand
    {
        std::array<uint8_t, 8> color = {0, 0, 0, 0, 0, 0, 0, 0};
    };

    std::array<std::array<Hand, board::ledclock::numHands>, 2> _buffer;
    uint8_t                                                    _activeBuffer = 0;

    static uint8_t rgbToColor(uint8_t r, uint8_t g, uint8_t b);

    auto &frontBuffer() { return _buffer[_activeBuffer]; }
    auto &backBuffer() { return _buffer[(_activeBuffer + 1) & 1]; }
};
