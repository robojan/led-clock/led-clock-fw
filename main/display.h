// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 March 06
 * Author: Robbert-Jan de Jager
 *
 * Declaration of the display class.
 ****************************************************************************/
#pragma once

#include "displayrenderer.h"
#include "idisplay.h"
#include "display/disableddisplay.h"
#include "display/displaymenu.h"
#include <vector>
#include <string_view>
#include <etl/debounce.h>
#include <memory>
#include <configuration.h>

class MatrixDriver;

class Display
{
public:
    Display(std::shared_ptr<Configuration> config);

    void registerDriver(MatrixDriver &driver);

    void tick();

    void addMenu(IMenuItem *menu);
    void addDisplay(IDisplayItem *display, bool active = false);

    const auto &displays() const { return _displayItems; }
    void        setActiveDisplay(int idx);


private:
    static constexpr int kDebounceValidCnt  = 2;
    static constexpr int kDebounceHoldCnt   = pdMS_TO_TICKS(1000);
    static constexpr int kDebounceRepeatCnt = pdMS_TO_TICKS(150);
    using debounce = etl::debounce<kDebounceValidCnt, kDebounceHoldCnt, kDebounceRepeatCnt>;

    enum class State
    {
        Display,
        MenuList,
        Menu,
    };

    DisplayRenderer             _renderer;
    std::vector<IMenuItem *>    _menuItems;
    int                         _activeMenu = 0;
    int                         _lastMenu   = -1;
    std::vector<IDisplayItem *> _displayItems;
    int                         _activeDisplay = 0;
    State                       _state         = State::Display;
    debounce                    _changeBtnDebounce;
    bool                        _changeSet = false;
    debounce                    _applyBtnDebounce;
    bool                        _applySet = false;

    std::unique_ptr<DisplayMenu>     _displayMenu;
    std::unique_ptr<DisabledDisplay> _disabledDisplay;

    void initButtons();
    void updateButtons();
    void processApplyButton(bool hold);
    void processChangeButton(bool hold);
    void renderMenuList();
};
