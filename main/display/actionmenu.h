// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 March 24
 * Author: Robbert-Jan de Jager
 *
 * This file declares a boolean setting menu
 ****************************************************************************/
#pragma once

#include "idisplay.h"
#include <string>
#include <etl/delegate.h>

class Display;

class ActionMenu : public IMenuItem
{
public:
    using Callback = etl::delegate<void()>;

    ActionMenu(Callback cb, std::string name) : _name(std::move(name)), _cb(cb) {}

    void activateMenu(DisplayRenderer &renderer) override { _cb(); }

    void deactivateMenu(DisplayRenderer &renderer) override {}

    void render(DisplayRenderer &renderer) override {}

    void             apply(bool hold) override {}
    void             change(bool hold) override {}
    std::string_view menuName() const override { return _name.c_str(); }

    bool isAction() const override { return true; }

private:
    std::string _name;
    Callback    _cb;
};
