// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 March 06
 * Author: Robbert-Jan de Jager
 *
 * This file contains the implementation of the display menu class
 ****************************************************************************/

#include "displaymenu.h"
#include "display.h"

DisplayMenu::DisplayMenu(std::shared_ptr<Configuration> config, Display &display)
    : _config(std::move(config)), _display(display)
{
}

void DisplayMenu::activateMenu(DisplayRenderer &renderer)
{
    _currentIdx = 0;
    _idxChanged = false;

    auto &displays = _display.displays();

    if(_currentIdx >= displays.size())
    {
        // Out of range print error
        renderer.setColor(1, 0, 0);
        renderer.setText("Error");
        return;
    }

    renderer.setColor(1, 1, 1);
    renderer.setText(displays[_currentIdx]->displayName());
}

void DisplayMenu::deactivateMenu(DisplayRenderer &renderer) {}

void DisplayMenu::render(DisplayRenderer &renderer)
{
    if(_idxChanged)
    {
        _idxChanged = false;

        auto &displays = _display.displays();

        if(_currentIdx >= displays.size())
        {
            // Out of range print error
            renderer.setColor(1, 0, 0);
            renderer.setText("Error");
            return;
        }
        else
        {
            renderer.setColor(1, 1, 1);
            renderer.setText(displays[_currentIdx]->displayName());
        }
    }
}

void DisplayMenu::apply(bool hold)
{
    // Mark the current display as the active one.
    _display.setActiveDisplay(_currentIdx);
    _config->set<ConfigId::ActiveDisplay>(_currentIdx);
}

void DisplayMenu::change(bool hold)
{
    // Go the the next option in the list
    _currentIdx = (_currentIdx + 1) % _display.displays().size();
    _idxChanged = true;
}
