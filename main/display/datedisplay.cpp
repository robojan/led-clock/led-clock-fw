// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 March 06
 * Author: Robbert-Jan de Jager
 *
 * This file contains the implementation of the Date Display class
 ****************************************************************************/

#include "datedisplay.h"
#include <time.h>

void DateDisplay::activateDisplay(DisplayRenderer &renderer)
{
    renderer.setColor(1, 1, 1);

    auto t    = time(nullptr);
    _lastDate = *localtime(&t);

    renderDate(renderer);
}

void DateDisplay::deactivateDisplay(DisplayRenderer &renderer)
{
    // Nothing to do
}

void DateDisplay::render(DisplayRenderer &renderer, bool force)
{
    auto t  = time(nullptr);
    auto dt = localtime(&t);

    // Skip when the date has not changed
    if(dt->tm_year == _lastDate.tm_year && dt->tm_mon == _lastDate.tm_mon &&
       dt->tm_mday == _lastDate.tm_mday && !force)
    {
        return;
    }

    _lastDate = *dt;

    renderDate(renderer);
}

void DateDisplay::renderDate(DisplayRenderer &renderer)
{
    auto &dt = _lastDate;

    char buffer[40];
    snprintf(buffer, sizeof(buffer), "%02d%02d%02d", dt.tm_mday, dt.tm_mon + 1, dt.tm_year % 100);

    renderer.setColor(1, 1, 1);
    renderer.setText(buffer);
}
