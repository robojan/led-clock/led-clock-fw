
#include <memory>
#include <optional>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>
#include <esp_err.h>
#include <esp_event.h>
#include <nvs_flash.h>
#include <nvs.h>
#include <driver/gpio.h>

#include <priorities.h>
#include <configuration.h>
#include <wifi_manager.h>
#include <dppkey.h>
#include <matrixdriver.h>
#include <display.h>
#include <clockrenderer.h>
#include <clockanimator.h>
#include <sntp.h>
#include <i2cmaster.h>
#include <peripherals/bh1721.h>
#include <peripherals/sht20.h>
#include <auto_ota.h>

#include <display/datedisplay.h>
#include <display/tempdisplay.h>
#include <display/humiditydisplay.h>
#include <display/deltadisplay.h>
#include <display/boolsettingmenu.h>
#include <display/actionmenu.h>


static const char *TAG = "Main";

struct MainState
{
    // Main Blocks
    std::shared_ptr<Configuration>                          config;
    std::optional<MatrixDriver>                             matrixDriver;
    std::optional<Display>                                  display;
    std::shared_ptr<ClockRenderer>                          clockRenderer;
    std::optional<ClockAnimator>                            clockAnimator;
    std::shared_ptr<driver::I2cMaster>                      i2cBus;
    std::shared_ptr<driver::Bh1721>                         ambientLightSensor;
    std::shared_ptr<driver::Sht20>                          humiditySensor;
    DateDisplay                                             dateDisplay;
    std::optional<TempDisplay>                              tempDisplay;
    std::optional<HumidityDisplay>                          humidityDisplay;
    std::optional<DeltaDisplay>                             significantDateDisplay;
    std::optional<BoolSettingMenu<ConfigId::OtaAutoUpdate>> autoUpdateMenu;
    std::optional<BoolSettingMenu<ConfigId::GradualHours>>  gradualHoursMenu;
    std::optional<AutoOta>                                  autoOta;
    std::optional<ActionMenu>                               reprovisionMenu;
    std::optional<ActionMenu>                               checkUpdateMenu;
};

static MainState g_state;

static void init_nvs()
{
    ESP_LOGI(TAG, "Initializing NVS");
    esp_err_t err = nvs_flash_init();
    if(err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_LOGW(TAG, "NVS partition could not be read (%x). Reinitializing it.", err);
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
}

static void init_sntp()
{
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_setservername(1, "time.google.com");
    sntp_init();
}

static void init_timezone()
{
    setenv("TZ", "CET-1CEST,M3.5.0,M10.5.0/3", 1);
    tzset();
}

static void init_i2cBus(MainState &s)
{
    s.i2cBus = std::make_shared<driver::I2cMaster>(board::i2c::i2cPort, board::i2c::sclPin,
                                                   board::i2c::sdaPin, board::i2c::baud,
                                                   priorities::isr::i2c);

    s.ambientLightSensor = std::make_shared<driver::Bh1721>(s.i2cBus);
    ESP_ERROR_CHECK_WITHOUT_ABORT(s.ambientLightSensor->init());

    s.humiditySensor = std::make_shared<driver::Sht20>(s.i2cBus);
    ESP_ERROR_CHECK_WITHOUT_ABORT(s.humiditySensor->init());
}

static void reprovision_cb()
{
    ESP_LOGI(TAG, "Starting reprovisioning");
    wifi_manager_disconnect_async();
}

static void update_cb()
{
    ESP_LOGI(TAG, "Do update");
    g_state.autoOta->forceCheck();
}

/**
 * @brief this is an exemple of a callback that you can setup in your own app to get notified of
 * wifi manager event.
 */
void cb_connection_ok(void *pvParameter)
{
    ip_event_got_ip_t *param = (ip_event_got_ip_t *)pvParameter;

    /* transform IP to human readable string */
    char str_ip[16];
    esp_ip4addr_ntoa(&param->ip_info.ip, str_ip, IP4ADDR_STRLEN_MAX);

    ESP_LOGI(TAG, "I have a connection and my IP is %s!", str_ip);
}

extern "C" void app_main()
{
    auto &s = g_state;

    ESP_LOGI(TAG, "Starting LED Clock App");

    // Create the default event loop
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // Initialize NVS
    nvs_flash_init();

    ESP_LOGI(TAG, "Starting networking");

    wifi_manager_start();

    /* register a callback as an example to how you can integrate your code with the wifi manager */
    wifi_manager_set_callback(WM_EVENT_STA_GOT_IP, &cb_connection_ok);

    // setup the global gpio isr service
    ESP_ERROR_CHECK(gpio_install_isr_service(priorities::isr::gpio));

    s.config = std::make_shared<Configuration>();

    ESP_LOGI(TAG, "Initing time");
    init_sntp();
    init_timezone();

    ESP_LOGI(TAG, "Init peripherals");
    init_i2cBus(s);


    ESP_LOGI(TAG, "Starting display");
    s.matrixDriver.emplace();
    s.display.emplace(s.config);
    s.clockRenderer = std::make_shared<ClockRenderer>();
    s.clockAnimator.emplace(s.clockRenderer);
    s.clockAnimator->setGradualHours(s.config->get_default<ConfigId::GradualHours>());

    s.display->registerDriver(*s.matrixDriver);
    s.matrixDriver->renderHandsCallback =
        etl::delegate<MatrixDriver::RenderFunc>::create<ClockRenderer, &ClockRenderer::renderFrame>(
            *s.clockRenderer);

    s.display->addDisplay(&s.dateDisplay, true);

    s.tempDisplay.emplace(s.humiditySensor);
    s.display->addDisplay(&s.tempDisplay.value(), false);

    s.humidityDisplay.emplace(s.humiditySensor);
    s.display->addDisplay(&s.humidityDisplay.value(), false);

    s.significantDateDisplay.emplace(1650454200);
    s.display->addDisplay(&s.significantDateDisplay.value(), false);

    s.autoUpdateMenu.emplace(s.config, "Auto update");
    s.display->addMenu(&s.autoUpdateMenu.value());

    s.gradualHoursMenu.emplace(s.config, "Gradual hours");
    s.display->addMenu(&s.gradualHoursMenu.value());

    s.reprovisionMenu.emplace(ActionMenu::Callback::create<reprovision_cb>(), "Connect to Wifi");
    s.display->addMenu(&s.reprovisionMenu.value());

    s.checkUpdateMenu.emplace(ActionMenu::Callback::create<update_cb>(), "Update");
    s.display->addMenu(&s.checkUpdateMenu.value());


    static constexpr int kDisplayVersion = 2;

    // Check if the displayversion has updated, if so reset the active display to 0 and mark the
    // last known display version.
    if(s.config->get_default<ConfigId::DisplayVersion>() != kDisplayVersion)
    {
        s.config->set<ConfigId::ActiveDisplay>(0);
        s.config->set<ConfigId::DisplayVersion>(kDisplayVersion);
    }

    s.display->setActiveDisplay(s.config->get_default<ConfigId::ActiveDisplay>());

    s.autoOta.emplace(1647993600, 60 * 60 * 24);
    if(s.config->get_default<ConfigId::OtaAutoUpdate>())
    {
        s.autoOta->start();
    }

    ESP_LOGI(TAG, "Started LED Clock App");

    s.matrixDriver->start();

    int i = 0;

    while(true)
    {
        s.clockAnimator->setGradualHours(s.config->get_default<ConfigId::GradualHours>());

        s.clockAnimator->tick();
        s.display->tick();
        s.humiditySensor->tick();

        if((i % pdMS_TO_TICKS(1000)) == 0)
        {
            auto [err, light] = s.ambientLightSensor->measure();
            if(err != ESP_OK)
            {
                ESP_LOGE(TAG, "Could not read ambient light: %s", esp_err_to_name(err));
            }

            auto temp     = s.humiditySensor->temperatureF();
            auto humidity = s.humiditySensor->relativeHumidityF();

            ESP_LOGI(TAG, "Ambient light: %ulx, temp %g deg C, humidity %g%%", light, temp,
                     humidity);
        }

        i++;
        vTaskDelay(1);
    }
}
