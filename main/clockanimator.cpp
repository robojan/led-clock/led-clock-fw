// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 March 01
 * Author: Robbert-Jan de Jager
 *
 * This file contains the implementation of the ClockAnimator class
 ****************************************************************************/

#include "clockanimator.h"
#include "clockrenderer.h"

ClockAnimator::ClockAnimator(std::shared_ptr<ClockRenderer> renderer)
    : _renderer(std::move(renderer))
{
}

void ClockAnimator::tick()
{
    auto currentTime = time(nullptr);
    if(currentTime != _lastTime)
    {
        struct tm *timeinfo;
        timeinfo = localtime(&currentTime);
        _renderer->clear();

        int hour_pos = (timeinfo->tm_hour % 12) * 5;
        if(_gradual_hours)
        {
            hour_pos += timeinfo->tm_min / 12;
        }

        for(int i = 0; i < 12; i++)
        {
            auto startR = i % 3 == 0 ? 5 : 6;
            _renderer->drawHand(i * 5, startR, 8, 1, 1, 1);
        }
        _renderer->drawHand(timeinfo->tm_sec, 0, 8, 0, 0, 1);
        _renderer->drawHand(timeinfo->tm_min, 0, 6, 0, 1, 0);
        _renderer->drawHand(hour_pos, 0, 4, 1, 0, 0);

        _renderer->swapBuffer();

        _lastTime = currentTime;
    }
}
