// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 February 27
 * Author: Robbert-Jan de Jager
 *
 * This file declares the LED matrix driver
 ****************************************************************************/
#pragma once

#include <driver/spi_master.h>
#include <etl/span.h>
#include <etl/delegate.h>
#include <userboard.h>
#include <freertos/semaphore.hpp>

class MatrixDriver
{
public:
    using RenderFunc = void(etl::span<std::byte> buffer, uint8_t columnIdx, uint8_t colorIdx);

    MatrixDriver();
    ~MatrixDriver();
    MatrixDriver(const MatrixDriver &) = delete;
    MatrixDriver(MatrixDriver &&)      = delete;

    MatrixDriver &operator=(const MatrixDriver &) = delete;
    MatrixDriver &operator=(MatrixDriver &&) = delete;

    void start();
    void stop();

    etl::delegate<RenderFunc> renderDisplayCallback;
    etl::delegate<RenderFunc> renderHandsCallback;

private:
    static constexpr uint16_t kTimerDivider = 16;
    static constexpr int      kTimerFreq    = 10000;
    static constexpr int      kFrameSize    = board::ledclock::frameSize / 8;

    spi_device_handle_t                              _spi = nullptr;
    spi_transaction_t                                _spiTrans{};
    std::array<std::array<std::byte, kFrameSize>, 2> _frameBuffers;
    uint8_t                                          _activeBuffer  = 0;
    bool                                             _taskRunning   = true;
    bool                                             _taskCompleted = false;
    TaskHandle_t                                     _task          = nullptr;
    freertos::BinarySemaphore                        _timerSemaphore;
    bool                                             _handsEnabled   = true;
    bool                                             _displayEnabled = true;
    uint8_t                                          _handsDriver    = 0;
    uint8_t                                          _displayDriver  = 0;

    auto &frontBuffer() { return _frameBuffers[_activeBuffer]; }
    auto &backBuffer() { return _frameBuffers[(_activeBuffer + 1) & 1]; }

    // Initialization functions
    void initSpi();
    void deinitSpi();
    void initIo();
    void deinitIo();
    void initTimer();
    void deinitTimer();
    void initTask();
    void deinitTask();

    // Render functions
    void createFrame();
    void createDriverFrame();
    void createDisplayFrame();
    void createHandsFrame();

    // Action functions
    // Reference to the frame must be kept alive while the transmission is in process.
    void send();
    void completeSend();
    void swapBuffers() { _activeBuffer = (_activeBuffer + 1) & 1; }


    // IO Pin functions
    void ioLatch() const;
    void ioRst() const;
    void ioEnableDrivers(bool enable) const;
    void ioEnableHands(bool enable) const;
    void ioEnableScreen(bool enable) const;

    // Callbacks
    void task();
    bool onTimerCallback();
};
