#pragma once

#include <soc/soc.h>
#include <driver/rmt.h>
#include <driver/gpio.h>
#include <driver/spi_master.h>
#include <driver/i2c.h>
#include <driver/timer.h>
#include <array>

namespace board
{
namespace dmaAllocations
{
}

namespace timerAllocations
{
static constexpr timer_group_t rendererGroup = TIMER_GROUP_0;
static constexpr timer_idx_t   rendererIdx   = TIMER_0;
} // namespace timerAllocations


namespace buttons
{

static constexpr gpio_num_t changeBtn = GPIO_NUM_34;
static constexpr gpio_num_t applyBtn  = GPIO_NUM_35;

static constexpr int                                numButtons = 2;
static constexpr std::array<gpio_num_t, numButtons> pins{changeBtn, applyBtn};
} // namespace buttons

namespace ledclock
{
static constexpr int numSeg        = 6;
static constexpr int ledsPerSeg    = 16;
static constexpr int numHands      = 60;
static constexpr int ledsPerHand   = 8;
static constexpr int handsPerGroup = 6;
static constexpr int ledsPerGroup  = ledsPerHand * handsPerGroup;
static constexpr int rows          = ledsPerSeg + ledsPerGroup;
static constexpr int numHandGroups = (numHands + handsPerGroup - 1) / handsPerGroup;
static constexpr int handCols      = numHandGroups * 3;
static constexpr int segCols       = numSeg * 3;
static constexpr int cols          = handCols + segCols;

#ifdef CONFIG_USERBOARD_PATCH_PCB

static constexpr uint16_t disabledSegmentValue = 0x0000;
static constexpr uint8_t  disabledHandValue    = 0x00;
static constexpr uint8_t  disabledDriverValue  = 0x00;

// Information about the shift register. The size and offset are in bits.
static constexpr int handGroupOffset = 0;
static constexpr int columnsOffset   = ledsPerGroup;
static constexpr int segmentsOffset  = columnsOffset + cols;
static constexpr int frameSize       = cols + ledsPerSeg + ledsPerGroup;

// Column chain:
// HG{8,9,10} <- HG{10,1,2} <- HG{3,5,4} <- SEG{6,5,4} <- SEG{4,3,2,1} <- SEG1,HG{7,6}

// 0 -> 16, 1 -> 17, 2 -> 0, 3 -> 1, 4->2, 5->3, 6 -> 4

static constexpr std::array<uint8_t, segCols> segColSeq{
    39, 36, 33, 30, 27, 24, // Red
    41, 37, 34, 31, 28, 25, // Green
    40, 38, 35, 32, 29, 26, // Blue
};

static constexpr std::array<uint8_t, handCols> handColSeq{
    9,  12, 15, 21, 18, 45, 42, 0, 3, 6, // Red
    10, 13, 16, 22, 19, 46, 43, 1, 4, 7, // Green
    11, 14, 17, 23, 20, 47, 44, 2, 5, 8, // Blue
};

static constexpr uint16_t segCharConv(uint16_t x)
{
    uint16_t result = 0;
    auto     map    = [&](int out, int in)
    {
        if((x & in) != 0)
            result |= out;
    };

    map(0x0001, 0x0200);
    map(0x0002, 0x1000);
    map(0x0004, 0x8000);
    map(0x0008, 0x4000);
    map(0x0010, 0x0020);
    map(0x0020, 0x0002);
    map(0x0040, 0x0004);
    map(0x0080, 0x0001);
    map(0x0100, 0x0100);
    map(0x0200, 0x2000);
    map(0x0400, 0x0010);
    map(0x0800, 0x0008);
    map(0x1000, 0x0080);
    map(0x2000, 0x0040);
    map(0x4000, 0x0400);
    map(0x8000, 0x0800);

    return result;
}

#else
static constexpr uint16_t disabledSegmentValue = 0xFFFF;
static constexpr uint8_t  disabledHandValue    = 0xFF;
static constexpr uint8_t  disabledDriverValue  = 0xFF;
static constexpr int      zeroHandGroupOffset  = 3;

// Information about the shift register. The size and offset are in bits.
static constexpr int handGroupOffset  = 0;
static constexpr int columnsOffset    = ledsPerGroup;
static constexpr int handColumnOffset = columnsOffset;
static constexpr int segColumnOffset  = columnsOffset + handCols;
static constexpr int segmentsOffset   = columnsOffset + cols;
static constexpr int frameSize        = cols + ledsPerSeg + ledsPerGroup;

static constexpr std::array<uint8_t, segCols> segColSeq{
    45, 42, 39, 36, 33, 30, // Red
    46, 43, 40, 37, 34, 31, // Green
    47, 44, 41, 38, 35, 32, // Blue
};

static constexpr std::array<uint8_t, handCols> handColSeq{
    21, 24, 27, 0, 3, 6, 9,  12, 15, 18, // Red
    22, 25, 28, 1, 4, 7, 10, 13, 16, 19, // Green
    23, 26, 29, 2, 5, 8, 11, 14, 17, 20, // Blue
};

static constexpr uint16_t segCharConv(uint16_t x)
{
    return x;
}

#endif

// Check for byte alignment
static_assert(segmentsOffset % 8 == 0);
static_assert(columnsOffset % 8 == 0);
static_assert(handGroupOffset % 8 == 0);

// IO information
static constexpr unsigned int      freq          = 20000000U;
static constexpr gpio_num_t        latchPin      = GPIO_NUM_18;
static constexpr gpio_num_t        rstPin        = GPIO_NUM_4;
static constexpr gpio_num_t        disScreenPin  = GPIO_NUM_21;
static constexpr gpio_num_t        disHandsPin   = GPIO_NUM_22;
static constexpr gpio_num_t        disDriversPin = GPIO_NUM_23;
static constexpr gpio_num_t        sckPin        = GPIO_NUM_5;
static constexpr gpio_num_t        dataPin       = GPIO_NUM_19;
static constexpr spi_host_device_t spi           = SPI3_HOST;

// Peripheral assignment
static constexpr timer_group_t timerGroup = board::timerAllocations::rendererGroup;
static constexpr timer_idx_t   timerIdx   = board::timerAllocations::rendererIdx;


inline static constexpr std::array<uint16_t, 256> createCharmap(
    std::initializer_list<std::pair<char, uint16_t>> chars)
{
    std::array<uint16_t, 256> result{};

    for(auto &c : result)
        c = disabledSegmentValue;

    for(auto &cm : chars)
    {
        auto c   = cm.first;
        auto val = segCharConv(cm.second);
        result[c] ^= val;
    }

    return result;
};

// Character map
static constexpr auto charmap = createCharmap({
    {'0', 0xff00}, {'1', 0x3000},  {'2', 0xeec0},
    {'3', 0xfcc0}, {'4', 0x31c0},  {'5', 0xddc0},
    {'6', 0xdfc0}, {'7', 0xf100},  {'8', 0xffc0},
    {'9', 0xfdc0}, {'A', 0xf3c0},  {'B', 0xdfc8},
    {'C', 0xcf00}, {'D', 0xfc12},  {'E', 0xcfc0},
    {'F', 0xc3c0}, {'G', 0xdf40},  {'H', 0x33c0},
    {'I', 0xcc12}, {'J', 0x3e00},  {'K', 0x0389},
    {'L', 0x0f00}, {'M', 0x332a},  {'N', 0x3321},
    {'O', 0xff00}, {'P', 0xe3c0},  {'Q', 0xff01},
    {'R', 0xe3c1}, {'S', 0xddc0},  {'T', 0xc012},
    {'U', 0x3f00}, {'V', 0x030c},  {'W', 0x3315},
    {'X', 0x002d}, {'Y', 0x002a},  {'Z', 0xcc0c},
    {'a', 0xfec0}, {'b', 0x1fc0},  {'c', 0x0ec0},
    {'d', 0x3ec0}, {'e', 0x0a84},  {'f', 0x40d2},
    {'g', 0xf5c0}, {'h', 0x13c0},  {'i', 0x0002},
    {'j', 0x3400}, {'k', 0x03c1},  {'l', 0x0412},
    {'m', 0x12c2}, {'n', 0x12c0},  {'o', 0x1ec0},
    {'p', 0xc388}, {'q', 0xf060},  {'r', 0x02c0},
    {'s', 0x0c41}, {'t', 0x04d2},  {'u', 0x1e00},
    {'v', 0x1001}, {'w', 0x1205},  {'x', 0x002d},
    {'y', 0x3dc0}, {'z', 0x0c84},  {'!' /* Degree*/, 0xe1c0},
    {'>', 0x0024}, {'<', 0x0009},  {'-', 0x00c0},
    {'/', 0x000c}, {'\\', 0x0021}, {'+', 0x00d2},
    {'=', 0x0880}, {'|', 0x0012},  {'_', 0x0c00},
    {'%', 0x95de}, {'*', 0x00ff},
});
} // namespace ledclock

namespace i2c
{
static constexpr i2c_port_t i2cPort = I2C_NUM_0;
static constexpr uint32_t   baud    = 100000;

static constexpr gpio_num_t sdaPin = GPIO_NUM_16;
static constexpr gpio_num_t sclPin = GPIO_NUM_17;

// BH1721FVC
static constexpr uint8_t lightSenserAddr = 0x23;

// SHT20
static constexpr uint8_t tempHumidityAddr = 0x40;

} // namespace i2c

namespace unused
{
static constexpr gpio_num_t boot    = GPIO_NUM_0;
static constexpr gpio_num_t debugTx = GPIO_NUM_1;
static constexpr gpio_num_t debugRx = GPIO_NUM_3;

static constexpr std::array<gpio_num_t, 5> reserved{GPIO_NUM_2, GPIO_NUM_25, GPIO_NUM_26,
                                                    GPIO_NUM_32, GPIO_NUM_33};

static constexpr gpio_num_t jtag_tck = GPIO_NUM_13;
static constexpr gpio_num_t jtag_tdo = GPIO_NUM_15;
static constexpr gpio_num_t jtag_tdi = GPIO_NUM_12;
static constexpr gpio_num_t jtag_tms = GPIO_NUM_14;
} // namespace unused

} // namespace board
