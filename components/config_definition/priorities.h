#pragma once

#include <esp_intr_alloc.h>

namespace priorities
{
namespace task
{
// Unused tasks:
static constexpr int stats = 0;

// Tasks:
static constexpr int matrixDriver         = 20;
static constexpr int matrixDriverAffinity = 1;
static constexpr int auto_ota             = 2;
} // namespace task

namespace isr
{
static constexpr int gpio        = ESP_INTR_FLAG_LEVEL2;
static constexpr int i2c         = ESP_INTR_FLAG_LEVEL2;
static constexpr int driverTimer = ESP_INTR_FLAG_LEVEL3;
} // namespace isr

} // namespace priorities
