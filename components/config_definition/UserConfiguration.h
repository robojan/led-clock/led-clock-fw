#pragma once

#include <typemap.h>
#include <string>
#include <esp_wifi.h>
#include <utility>
#include <staticmap.h>
#include <networksettings.h>

// Don't change numeric values of this Enum to remain backwards compatible.
enum class ConfigId
{
    // Network settings
    WifiStaSsid     = 0,
    WifiStaMode     = 1,
    WifiStaPassword = 2,
    WifiStaAuthMode = 3,
    WifiApSsid      = 4,
    WifiApPassword  = 5,
    WifiApMode      = 6,
    WifiApAuthMode  = 7,
    WifiApHidden    = 8,
    WifiApChannel   = 9,
    Hostname        = 10,

    // OTA settings
    OtaAutoUpdate = 100,

    // Ledclock settings
    ActiveDisplay  = 200,
    DisplayVersion = 201,
    GradualHours   = 202,
};

using ConfigTypeMap = type_map<ConfigId,
                               ct_pair<ConfigId::WifiStaSsid, std::string>,
                               ct_pair<ConfigId::WifiStaMode, netconfig::StationMode>,
                               ct_pair<ConfigId::WifiStaPassword, std::string>,
                               ct_pair<ConfigId::WifiStaAuthMode, wifi_auth_mode_t>,
                               ct_pair<ConfigId::WifiApSsid, std::string>,
                               ct_pair<ConfigId::WifiApPassword, std::string>,
                               ct_pair<ConfigId::WifiApMode, netconfig::ApMode>,
                               ct_pair<ConfigId::WifiApAuthMode, wifi_auth_mode_t>,
                               ct_pair<ConfigId::WifiApHidden, bool>,
                               ct_pair<ConfigId::WifiApChannel, uint8_t>,
                               ct_pair<ConfigId::Hostname, std::string>,
                               ct_pair<ConfigId::OtaAutoUpdate, bool>,
                               ct_pair<ConfigId::ActiveDisplay, int>,
                               ct_pair<ConfigId::DisplayVersion, int>,
                               ct_pair<ConfigId::GradualHours, bool>>;

static constexpr StaticMap ConfigDefaultMap{
    makeStaticMapEntry<ConfigId::WifiStaMode>(netconfig::StationMode::Dpp),
    makeStaticMapEntry<ConfigId::WifiStaSsid>(""),
    makeStaticMapEntry<ConfigId::WifiStaPassword>(""),
    makeStaticMapEntry<ConfigId::WifiStaAuthMode>(WIFI_AUTH_OPEN),

    makeStaticMapEntry<ConfigId::WifiApMode>(netconfig::ApMode::Off),
    makeStaticMapEntry<ConfigId::WifiApSsid>("LED Clock"),
    makeStaticMapEntry<ConfigId::WifiApPassword>("password_LED_Clock"),
    makeStaticMapEntry<ConfigId::WifiApAuthMode>(WIFI_AUTH_WPA2_PSK),
    makeStaticMapEntry<ConfigId::WifiApHidden>(false),
    makeStaticMapEntry<ConfigId::WifiApChannel>(8),

    makeStaticMapEntry<ConfigId::Hostname>("LED_Clock"),
    makeStaticMapEntry<ConfigId::OtaAutoUpdate>(true),

    makeStaticMapEntry<ConfigId::ActiveDisplay>(0),
    makeStaticMapEntry<ConfigId::DisplayVersion>(0),
    makeStaticMapEntry<ConfigId::GradualHours>(true),
};
